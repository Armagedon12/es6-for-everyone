import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  //TODO: add rectangles above details
  const attackElement = createElement({ tagName: 'span', className: 'fighter-details' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-details' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-details' });

  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  fighterDetails.append(imgElement);

  nameElement.innerText = name.toUpperCase();
  fighterDetails.append(nameElement);

  attackElement.innerText = `Hit power: ${attack}`;
  fighterDetails.append(attackElement);

  defenseElement.innerText = `Block power: ${defense}`;
  fighterDetails.append(defenseElement);

  healthElement.innerText = `HP: ${health}`;
  fighterDetails.append(healthElement);

  return fighterDetails;
}
