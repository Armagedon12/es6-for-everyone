import { showModal } from "./modal";
import {createElement} from "../helpers/domHelper";

export function showWinnerModal(fighter) {
  // show winner name and image
    const title = 'Winner';
    const bodyElement = createWinner(fighter);
    showModal({ title, bodyElement });
}

function createWinner(fighter) {
    const { name, source } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

    const attributes = { src: source };
    const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
    fighterDetails.append(imgElement);

    nameElement.innerText = name.toUpperCase();
    fighterDetails.append(nameElement);

    return fighterDetails;
}