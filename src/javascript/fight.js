export function fight(firstFighter, secondFighter) {
    while (true) {
        // First fighter hits
        reduceHealth(secondFighter, getDamage(firstFighter, secondFighter));
        if (getHealth(firstFighter) <= 0) {
            return secondFighter;
        }

        // Second fighter hits
        reduceHealth(firstFighter, getDamage(secondFighter, firstFighter));
        if (getHealth(secondFighter) <= 0) {
            return firstFighter;
        }
    }
}

export function getDamage(attacker, enemy) {
    return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
    return fighter.attack;
}

export function getBlockPower(fighter) {
    return fighter.defense;
}

export function getHealth(fighter) {
    return fighter.health;
}

export function reduceHealth(fighter, damage) {
    fighter.health -= damage;
}
